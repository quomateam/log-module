<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use quoma\modules\log\LogModule;

/* @var $this yii\web\View */
/* @var $model \quoma\modules\log\models\Log */

$user = \webvimark\modules\UserManagement\models\User::findOne($model->user_id);
$username = '';
if($user){
    $username = $user->username;
}

$this->title = Yii::$app->formatter->asDatetime($model->datetime);
$this->params['breadcrumbs'][] = ['label' => LogModule::t('Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-view">

    <h1><?= Html::encode($this->title) ?> <small><?= $username ?></small></h1>

    <div class="alert alert-info"><h4><?php
    
    if($model->model_id){
        echo Yii::t('logs', 'The user "{user}" accessed to "{route}" with id "{id}" at "{datetime}"', [
            'user' => $username,
            'route' => Yii::t('logs', $model->route),
            'id' => $model->model_id,
            'datetime' => Yii::$app->formatter->asDatetime($model->datetime)
        ]);
    }else{
        echo Yii::t('logs', 'The user "{user}" accessed to "{route}" at "{datetime}"', [
            'user' => $username,
            'route' => Yii::t('logs', $model->route),
            'id' => $model->model_id,
            'datetime' => Yii::$app->formatter->asDatetime($model->datetime)
        ]);
    }
    ?></h3></div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'log_id',
            [
                'attribute' => 'route',
                'value' => Yii::t('logs', $model->route)
            ],
            [
                'attribute' => 'user_id',
                'value' => $username
            ],
            'datetime:datetime',
            'model',
            'model_id',
            [
                'attribute'=> 'attribute',
                'label' => LogModule::t('Modified attributes'),
                'format' => 'html',
                'value' => Html::tag('pre', print_r($model->attribute, true)),
            ],
        ],
    ]) ?>

    <table class="table">
        <thead>
            <tr>
            <th scope="col"><?= LogModule::t('Attribute') ?></th>
            <th scope="col"><?= LogModule::t('Old Value') ?></th>
            <th scope="col"><?= LogModule::t('New Value') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php

            function format($name, $val) {
                $formatteables = [
                    'created_at' => 'datetime',
                    'updated_at' => 'datetime',
                    'deleted_at' => 'datetime',
                ];

                if(isset($formatteables[$name])){
                    $format = $formatteables[$name];
                    return Yii::$app->formatter->format($val, $format);
                }

                return $val;
            }

            $attributes = json_decode($model->attribute);
            if($attributes && is_iterable($attributes)){
                $old = json_decode($model->old_value);
                $new = json_decode($model->new_value);

                foreach($attributes as $k => $attr): ?>
                <tr>
                    <td><?= Yii::t('app', ucwords(yii\helpers\Inflector::humanize($attr))) ?> (<?= $attr ?>)</td>
                    <td><?= $old[$k] ? format($attr, $old[$k]) : '-' ?></td>
                    <td><?= $new[$k] ? format($attr, $new[$k]) : '-' ?></td>
                </tr>
                <?php
                endforeach;
            }
            ?>

        </tbody>
    </table>
</div>
